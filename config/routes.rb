Rails.application.routes.draw do
  # get 'welcome/index', to: 'welcome#index'

  resources :channels
  resources :districts
  resources :cities
  resources :provinces
  resources :welcome

  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
