json.extract! crud, :id, :created_at, :updated_at
json.url crud_url(crud, format: :json)
