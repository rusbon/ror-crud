json.extract! channel, :id, :company, :address, :district_id, :long, :lat, :status, :created_at, :updated_at
json.url channel_url(channel, format: :json)
