class City < ApplicationRecord
  belongs_to :province

  attr_accessor :province_name
end
