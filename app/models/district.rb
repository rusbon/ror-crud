class District < ApplicationRecord
  belongs_to :city

  attr_accessor :city_name
end
