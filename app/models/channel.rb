class Channel < ApplicationRecord
  belongs_to :district

  attr_accessor :district_name
end
