class WelcomeController < ApplicationController
  def index
    @provinces = Province.all
    @cities = City.all
    @districts = District.all
    @channels = Channel.all
  end
end
