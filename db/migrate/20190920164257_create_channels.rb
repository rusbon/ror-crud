class CreateChannels < ActiveRecord::Migration[5.1]
  def change
    create_table :channels do |t|
      t.string :company
      t.string :address
      t.belongs_to :district, foreign_key: true
      t.string :long
      t.string :lat
      t.string :status

      t.timestamps
    end
  end
end
